import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './Components/Layout';
import Home from './Components/Home';
import LinkUpdater from "./Components/LinkUpdater";

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route exact path='/Home' component={Home} />
        <Route exact path='/LinkUpdater' component={LinkUpdater} />
      </Layout>
    );
  }
}
