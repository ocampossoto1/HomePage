import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { NavMenu } from './NavMenu';
//style={{backgroundColor: "#121212",  flex: 1}}
export class Layout extends Component {
  static displayName = Layout.name;

  render () {
    return (
      <div >
        <NavMenu />
        <Container >
          {this.props.children}
        </Container>
      </div>
    );
  }
}
