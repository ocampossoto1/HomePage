import React from "react";
import {Row, Col, Card, CardColumns } from "react-bootstrap";
import SettingsIcon from '@material-ui/icons/Settings';

export default class Links extends React.Component{
    constructor(props){
        super(props);
        this.state={LinksList : [], viewType: "group"}
    }
    componentDidMount(){
        var list = localStorage.getItem('myData')? JSON.parse(localStorage.getItem('myData')) : [];
        // for(var i in localStorage.getItem('myData')){
        //     list.push(data[i]);
        // }
        this.setState({LinksList: list});
    }
    DiplayCardview = () => {
        if(this.state.viewType === "group"){
            
        }
    }
    SetView = () => {
        
    }
    DisplayLinks = () =>{
        return;
    }
    render(){
        return (
            <div>           
                <Row>
                    <Col md="4">
                        <h1 style={{color: "white"}}>Current Links</h1>
                    </Col>
                    <Col md={{offset: 6}}>
                        <SettingsIcon style={{color: "grey"}}/>
                    </Col>
                </Row>
                <CardColumns>
                {this.state.LinksList.map((link, key) => 
                    <Card bg="dark" text="white" key={key} onClick={()=> window.open(link.URL, "_blank")}>
                        {/* <Card.Img variant="top" src="holder.js/100px160" /> */}
                        <Card.Body>
                            <Card.Title>{link.Name}</Card.Title>
                            <Card.Text>
                                {link.URL}
                            </Card.Text>
                        </Card.Body>

                    </Card>
                )}
                </CardColumns>

            </div>

            
        );
    }
}