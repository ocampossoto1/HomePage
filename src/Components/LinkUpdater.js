import React from "react";
import {Row, Col, Button, Modal} from "react-bootstrap";
import {Link} from 'react-router-dom';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
var textColor = {color: "white"};
export default class LinkUpdater extends React.Component{
    constructor(props){
        super(props);
        var dataList = localStorage.getItem('myData')? JSON.parse(localStorage.getItem('myData')) : [];
        this.state={data: dataList, linkName: "", linkurl: "", showModal: false, NewlinkName: "", newlinkurl:"", editId: null };
    }
    addNewLink = () => {
        var data = this.state.data;
        data[this.state.data.length] = {"Name": this.state.linkName, "URL": this.state.linkurl};
        this.setState({data: data}, () => {
            localStorage.setItem('myData', JSON.stringify(this.state.data));
        });
    }
    RemoveItem = (item) => {
        var newList = this.state.data;
        delete newList[item];
        newList = newList.filter(function (el) {
            return el != null;
          });
        this.setState({data: newList}, () => {
            localStorage.setItem('myData', JSON.stringify(this.state.data));
        });
    }
    EditThisToggle = (value) =>{
        this.setState({NewlinkName: this.state.data[value].Name, Newlinkurl: this.state.data[value].URL, showModal: !this.state.showModal, editId: value});
    }
    SaveEditChanges = () =>{
        var data = this.state.data;
        data[this.state.editId] = {"Name": this.state.NewlinkName, "URL": this.state.Newlinkurl};
        this.setState({data: data, showModal: !this.state.showModal}, () => {
            localStorage.setItem('myData', JSON.stringify(this.state.data));
        });
    }
    ToggleEditModal = () => {
        this.setState({showModal: !this.state.showModal});
    }
    render(){
        return <div >
            
        <Row>
            <Col md="1"><label style={textColor}>Name: </label></Col>
            <Col md="2"><input onChange={(e) => this.setState({linkName : e.target.value})} type="text"/></Col>
            <Col md="1"><label style={textColor}>URL: </label></Col>
            <Col md="2"><input onChange={(e) => this.setState({linkurl : e.target.value})} type="text"/></Col>
            <Col md="2"><Button onClick={this.addNewLink}>Add</Button></Col>
        </Row>
        <Row>
            <Col><h2 style={textColor}>Current Links</h2></Col>
        </Row>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                <th scope="col" style={textColor}>Name</th>
                <th scope="col" style={textColor}>URL</th>
                <th scope="col" style={textColor}>Edit</th>
                </tr>
            </thead>
            <tbody>
            {this.state.data.map((item, key) => 
                <tr key={key}>
                    <td md="2">
                        <Link onClick={()=> window.open(item.URL, "_blank")} style={{color: "white", textDecoration: "underline"}}> {item.Name}</Link>
                    </td>
                    <td>
                        <label style={textColor}>{item.URL}</label>
                    </td>
                    <td>
                        <Row>
                            <Col>
                                <Button onClick={() => this.EditThisToggle(key)}><EditIcon/></Button> 
                            </Col>
                            <Col>  
                                <Button onClick={() => this.RemoveItem(key)} variant="danger"> <DeleteForeverIcon/></Button>
                            </Col>
                        </Row>
                    </td>
                </tr>
                )}
            </tbody>
            </table>
            <Modal show={this.state.showModal} onHide={this.ToggleEditModal}>
                <Modal.Header closeButton>
                <Modal.Title><h1>Edit Link</h1></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col md="2"><p>Name: </p></Col>
                        <Col md="10"><input value={this.state.NewlinkName} onChange={(e) => this.setState({NewlinkName : e.target.value})} style={{width: "100%"}} type="text"/></Col>
                       
                    </Row>
                    <Row>
                        <Col md="2"><label>URL: </label></Col>
                        <Col md="10"><input value={this.state.Newlinkurl} onChange={(e) => this.setState({Newlinkurl : e.target.value})} style={{width: "100%"}} type="text"/></Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={this.ToggleEditModal}>
                    Close
                </Button>
                <Button variant="primary" onClick={this.SaveEditChanges}>
                    Save Changes
                </Button>
                </Modal.Footer>
            </Modal>
        </div>
    }
}